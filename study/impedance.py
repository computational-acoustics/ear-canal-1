from pathlib import Path
import numpy as np
from scipy.interpolate import PchipInterpolator

# Data from Measurement of the eardrum impedance of human ears, Herbert Hudde


_data_path = Path(__file__).parent.joinpath('data', 'eardrum-impedance')

_reactance = np.genfromtxt(_data_path.joinpath('Reactance.csv'), delimiter=',')
_resistance = np.genfromtxt(_data_path.joinpath('Resistance.csv'), delimiter=',')


_reactance_interpolator = PchipInterpolator(
    x=_reactance[:, 0],
    y=_reactance[:, 1],
    axis=0,
    extrapolate=True
)


_resistance_interpolator = PchipInterpolator(
    x=_resistance[:, 0],
    y=_resistance[:, 1],
    axis=0,
    extrapolate=True
)


def plane_wave_specific_impedance(density: float, speed_of_sound: float) -> complex:
    """
    Calculate the specific acoustic impedance for a plane wave.

    :param density: The medium density, in kilogram per cubic meter.
    :param speed_of_sound: The phase speed of sound in the medium, in meter per second.
    :return: The impedance in MKS Rayl.
    """
    return (density * speed_of_sound) + 0j


def plane_wave_impedance(density: float, speed_of_sound: float, area: float) -> complex:
    """
    Calculate the acoustic impedance for a plane wave.

    :param density: The medium density, in kilogram per cubic meter.
    :param speed_of_sound: The phase speed of sound in the medium, in meter per second.
    :param area: Area of the surface at which to compute the impedance, in square meter.
    :return: The impedance in MKS acoustic Ohm.
    """
    return plane_wave_specific_impedance(density=density, speed_of_sound=speed_of_sound) / area


def eardrum_impedance(frequency: float, density: float, speed_of_sound: float, area: float) -> complex:
    """
    Calculate the "eardrum" acoustic impedance, i.e. the acoustic impedance of the ear canal at a representative
    plane 2-3 mm from the eardrum according to
    [Measurement of the eardrum impedance of human ears](https://pubmed.ncbi.nlm.nih.gov/6826891/) by Herbert Hudde.

    :param frequency: The frequency at which to compute the impedance, in Hertz.
    :param density: The medium density, in kilogram per cubic meter.
    :param speed_of_sound: The phase speed of sound in the medium, in meter per second.
    :param area: Area of the ear canal section at the representative plane, in square meter.
    :return: The impedance in MKS acoustic Ohm.
    """
    return (
            plane_wave_impedance(density=density, speed_of_sound=speed_of_sound, area=area) *
            (_resistance_interpolator(frequency).item() + 1j * _reactance_interpolator(frequency).item())
    )


def eardrum_specific_impedance(frequency: float, density: float, speed_of_sound: float) -> complex:
    """
    Calculate the "eardrum" specific acoustic impedance, i.e. the acoustic impedance of the ear canal at a
    representative plane 2-3 mm from the eardrum according to
    [Measurement of the eardrum impedance of human ears](https://pubmed.ncbi.nlm.nih.gov/6826891/) by Herbert Hudde.

    :param frequency: The frequency at which to compute the impedance, in Hertz.
    :param density: The medium density, in kilogram per cubic meter.
    :param speed_of_sound: The phase speed of sound in the medium, in meter per second.
    :return: The impedance in MKS Rayl.
    """
    return (
            (density * speed_of_sound + 0j) *
            (_resistance_interpolator(frequency).item() + 1j * _reactance_interpolator(frequency).item())
    )


def elmer_eardrum_impedance(frequency: float, speed_of_sound: float) -> complex:
    """
    Calculate the "eardrum" `Wave Impedance` quantity required by Elmer's `HelmholtzSolver` solver for an impedance
    boundary condition. This impedance is computed in the ear canal at a representative plane 2-3 mm from the eardrum
    according to
    [Measurement of the eardrum impedance of human ears](https://pubmed.ncbi.nlm.nih.gov/6826891/) by Herbert Hudde.

    :param frequency: The frequency at which to compute the impedance, in Hertz.
    :param speed_of_sound: The phase speed of sound in the medium, in meter per second.
    :return: The impedance in meter per second.
    """
    return speed_of_sound * (
        _resistance_interpolator(frequency).item() + 
        1j * _reactance_interpolator(frequency).item()
    )
