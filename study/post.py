from pathlib import Path
import json
import meshio
import numpy as np
from dataclasses import dataclass
import shutil
from pywebplots.config import BasicConfig
from pywebplots import plot as plt
from pywebplots import file as plt_file
import plotly
from fractions import Fraction, Decimal
from study import impedance


@dataclass(frozen=True)
class Post:
    """
    A container for the processed simulation info. The `frequency` array contains frequency values. All the other
    values are intended as functions of this frequency.

    :param frequency: The array of the frequencies of each simulation, in Hertz.
    :param velocity: The array of velocities of each simulation, in meter per second.
    :param inlet_pressure: The array of complex average pressure over the ear canal inlet, in pascal.
    :param inlet_spl: The array of average SPL over the ear canal inlet, in dBSPL.
    :param termination_pressure: The array of complex average pressure over the ear canal termination, in pascal.
    :param termination_spl: The array of average SPL over the ear canal termination, in dBSPL.
    :param pressure_frequency_response: The pressure frequency response between inlet and outlet, dimensionless.
    :param velocity_frequency_response: The velocity frequency response between inlet and outlet, Pascal over meter per second.
    """
    frequency: np.ndarray
    velocity: np.ndarray
    inlet_pressure: np.ndarray
    inlet_spl: np.ndarray
    termination_pressure: np.ndarray
    termination_spl: np.ndarray
    pressure_frequency_response: np.ndarray
    velocity_frequency_response: np.ndarray


def _face_selector(grid: np.ndarray, mesh: meshio.Mesh, faces_list: list[list[str]]) -> np.ndarray:
    termination_tags = np.array([k for k in mesh.cell_tags if mesh.cell_tags[k] in faces_list])
    triangle_cell_tags = mesh.get_cell_data(name='cell_tags', cell_type='triangle')
    triangle_cell_idx = mesh.cells_dict['triangle']
    sel_termination = np.zeros(grid.shape[0], dtype=bool)

    for t_tag in termination_tags:
        sel_cells = triangle_cell_tags == t_tag
        idx = triangle_cell_idx[sel_cells, :].flatten()
        sel_termination[idx] = True

    return sel_termination


@dataclass(frozen=True)
class _FieldExtraction:
    field_at_centroid: complex
    average_field_over_selection: complex
    spl_at_centroid: float
    average_spl_over_selection: float


def _harmonic_spl(field: complex | np.ndarray) -> float:
    return 20.0 * np.log10(np.abs(field) / (20e-6 * np.sqrt(2)))


def _field_extractor(grid: np.ndarray, node_selector: np.ndarray, field: np.ndarray) -> _FieldExtraction:
    assert grid.ndim == 2
    assert grid.shape[1] == 3
    assert node_selector.ndim == 1
    assert field.ndim == 2
    assert field.shape[1] == 1
    assert grid.shape[0] == node_selector.size == field.shape[0]

    selected_grid = grid[node_selector, :]
    centroid = np.nanmean(selected_grid, axis=0)
    gaps = selected_grid - centroid.reshape((1, 3))
    distances = np.sqrt(np.nansum(gaps**2, axis=1))
    centroid_node_idx = np.argmin(distances)

    field_at_centroid = field[node_selector, 0][centroid_node_idx]
    field_over_selection = field[node_selector, 0]
    average_field_over_selection = np.nanmean(field_over_selection)

    spl_at_centroid = _harmonic_spl(field_at_centroid)
    spl_over_selection = _harmonic_spl(field_over_selection)
    average_spl_over_selection = np.nanmean(spl_over_selection)

    return _FieldExtraction(
        field_at_centroid=field_at_centroid,
        average_field_over_selection=average_field_over_selection,
        spl_at_centroid=spl_at_centroid,
        average_spl_over_selection=average_spl_over_selection
    )


def process(data_dir: Path) -> Post:
    """
    Process all the data in a simulations directory, returning a `Post` object.

    :param data_dir: The simulation data directory.
    :return: The `Post` object.
    """
    assert data_dir.is_dir()

    source_mesh = meshio.read(Path(__file__).parent.joinpath('data', 'mesh', 'mesh_20kHz.med'))

    grid = source_mesh.points / 1000.0

    inlet_faces = [
        ['inlet'],
    ]
    sel_inlet = _face_selector(grid=grid, mesh=source_mesh, faces_list=inlet_faces)

    termination_faces = [
        ['outlet'],
    ]
    sel_termination = _face_selector(grid=grid, mesh=source_mesh, faces_list=termination_faces)

    freq_axis = []
    vel_axis = []
    inlet_pressure = []
    inlet_spl = []
    termination_pressure = []
    termination_spl = []
    pressure_transfer_function = []
    velocity_transfer_function = []

    for p in data_dir.rglob('*.*'):
        if not p.is_dir():
            continue

        meta_path = p.joinpath('simdata.json')
        if not meta_path.exists():
            continue

        mesh_path = p.joinpath('case_t0001.vtu')
        if not mesh_path.exists():
            continue

        with open(meta_path, 'r') as meta_file:
            meta = json.load(meta_file)

        frequency = meta['frequency']
        velocity = meta['velocity_re'] + 1j * meta['velocity_im']

        mesh = meshio.read(mesh_path)
        assert np.allclose(mesh.points, grid)

        field = mesh.point_data['pressure wave 1'] + 1j * mesh.point_data['pressure wave 2']
        assert len(field.shape) == 2
        assert field.shape[1] == 1

        inlet_field_extraction = _field_extractor(
            grid=mesh.points,
            node_selector=sel_inlet,
            field=field
        )

        termination_field_extraction = _field_extractor(
            grid=mesh.points,
            node_selector=sel_termination,
            field=field
        )

        freq_axis.append(frequency)
        vel_axis.append(velocity)
        inlet_pressure.append(inlet_field_extraction.average_field_over_selection)
        inlet_spl.append(inlet_field_extraction.average_spl_over_selection)
        termination_pressure.append(termination_field_extraction.average_field_over_selection)
        termination_spl.append(termination_field_extraction.average_spl_over_selection)

        pressure_transfer_function.append(
            termination_field_extraction.average_field_over_selection /
            inlet_field_extraction.average_field_over_selection
        )
        velocity_transfer_function.append(
            termination_field_extraction.average_field_over_selection /
            velocity
        )

    freq_arr = np.array(freq_axis)
    idx = np.argsort(freq_arr)

    return Post(
        frequency=freq_arr[idx],
        velocity=np.array(vel_axis)[idx],
        inlet_pressure=np.array(inlet_pressure)[idx],
        inlet_spl=np.array(inlet_spl)[idx],
        termination_pressure=np.array(termination_pressure)[idx],
        termination_spl=np.array(termination_spl)[idx],
        pressure_frequency_response=np.array(pressure_transfer_function)[idx],
        velocity_frequency_response=np.array(velocity_transfer_function)[idx]
    )


def plot_post(post: Post, dst: Path) -> None:
    """
    Produce a plot of the frequency response.

    :param post: The container of the post-processed simulation.
    :param dst: The directory where to deploy the plot.
    :return: Nothing.
    """
    assert dst.is_dir()

    fig = plt.subplots(basic_config=BasicConfig(), rows=2, cols=1, shared_xaxes=True)

    resp_db = 20.0 * np.log10(np.abs(post.pressure_frequency_response))
    plt.add_line_subplot(
        fig=fig,
        row=1,
        col=1,
        x=post.frequency,
        y=resp_db,
        name='Magnitude',
        showlegend=False,
        marker=dict(color=plotly.colors.qualitative.Plotly[0])
    )

    resp_rad = np.angle(post.pressure_frequency_response)
    plt.add_line_subplot(
        fig=fig,
        row=2,
        col=1,
        x=post.frequency,
        y=resp_rad,
        name='Phase',
        showlegend=False,
        marker=dict(color=plotly.colors.qualitative.Plotly[0])
    )

    fig.update_xaxes(
        type='log',
        title_text='Frequency [Hz]',
        row=1,
        col=1
    )

    db_res = 5
    db_min = db_res * np.floor(np.min(resp_db) / db_res)
    db_max = db_res * np.ceil(np.max(resp_db) / db_res)
    db_ticks = np.arange(db_min, db_max + db_res, db_res)
    fig.update_yaxes(
        title_text='Magnitude [dB]',
        tickvals=db_ticks,
        range=[db_min, db_max],
        row=1,
        col=1
    )

    fig.update_xaxes(
        type='log',
        title_text='Frequency [Hz]',
        row=2,
        col=1
    )

    phs_res = np.deg2rad(45)
    phs_ticks = np.arange(-np.pi, np.pi + phs_res, phs_res)
    phs_ticks_text = [f'{Fraction.from_decimal(Decimal(p / np.pi))} π' for p in phs_ticks]
    fig.update_yaxes(
        title_text='Phase [rad]',
        tickvals=phs_ticks,
        ticktext=phs_ticks_text,
        range=[-np.pi, np.pi],
        row=2,
        col=1
    )

    plt_file.save_plot_with_script(fig=fig, dst=dst.joinpath('pressure_frequency_response.html'))


def animate(data_dir: Path, anim_dir: Path) -> None:
    """
    Produce a ParaView animation of the field in the ear canal.

    :param data_dir: The simulation data directory.
    :param anim_dir: The directory where to deploy the animation.
    :return: Nothing.
    """

    data_paths: list[Path] = []
    frequencies: list[float] = []
    for p in data_dir.rglob('*.*'):

        meta_path = p.joinpath('simdata.json')
        if not meta_path.exists():
            continue

        with open(meta_path, 'r') as meta_file:
            meta = json.load(meta_file)

        mesh_path = p.joinpath('case_t0001.vtu')
        if not mesh_path.exists():
            continue

        data_paths.append(mesh_path)
        frequencies.append(meta['frequency'])

    idx = np.argsort(frequencies)

    for n in range(idx.size):
        shutil.copy(
            src=data_paths[idx[n]],
            dst=anim_dir.joinpath(f'case_t{(n + 1):0>4}.vtu'),
            follow_symlinks=False
        )

        with open(anim_dir.joinpath(f'freq_{n+1}.csv'), 'w') as file:
            file.write(f'{frequencies[idx[n]]:.30f}\n')


def plot_eardrum_specific_impedance(dst: Path) -> None:
    """
    Plot the eardrum specific impedance according to [Measurement of the eardrum impedance of human ears](https://pubmed.ncbi.nlm.nih.gov/6826891/) by Herbert Hudde.

    :param dst: The directory where to deploy the plot.
    :return: Nothing.
    """

    assert dst.is_dir()

    frequencies = np.unique(np.round(np.logspace(3, np.log10(20e3), num=2048)))
    impedance_values = np.array(
        [
            impedance.eardrum_specific_impedance(frequency=f, speed_of_sound=343.0, density=1.205)
            for f in frequencies
        ]
    )

    mag = np.abs(impedance_values)
    phs = np.angle(impedance_values)

    fig = plt.subplots(basic_config=BasicConfig(), rows=2, cols=1, shared_xaxes=True)

    plt.add_line_subplot(
        fig=fig,
        row=1,
        col=1,
        x=frequencies,
        y=mag,
        name='Magnitude',
        showlegend=False,
        marker=dict(color=plotly.colors.qualitative.Plotly[0])
    )

    plt.add_line_subplot(
        fig=fig,
        row=2,
        col=1,
        x=frequencies,
        y=phs,
        name='Phase',
        showlegend=False,
        marker=dict(color=plotly.colors.qualitative.Plotly[0])
    )

    fig.update_xaxes(
        type='log',
        title_text='Frequency [Hz]',
        row=1,
        col=1
    )

    # mag_res = 100
    # mag_min = mag_res * np.floor(np.min(mag) / mag_res)
    # mag_max = mag_res * np.ceil(np.max(mag) / mag_res)
    # mag_ticks = np.arange(mag_min, mag_max + mag_res, mag_res)
    fig.update_yaxes(
        title_text='Magnitude [MKS Rayl]',
        # tickvals=mag_ticks,
        # range=[mag_min, mag_max],
        row=1,
        col=1
    )

    fig.update_xaxes(
        type='log',
        title_text='Frequency [Hz]',
        row=2,
        col=1
    )

    phs_res = np.deg2rad(45)
    phs_ticks = np.arange(-np.pi, np.pi + phs_res, phs_res)
    phs_ticks_text = [f'{Fraction.from_decimal(Decimal(p / np.pi))} π' for p in phs_ticks]
    fig.update_yaxes(
        title_text='Phase [rad]',
        tickvals=phs_ticks,
        ticktext=phs_ticks_text,
        range=[-np.pi, np.pi],
        row=2,
        col=1
    )

    plt_file.save_plot_with_script(fig=fig, dst=dst.joinpath('eardrum_specific_impedance.html'))


def run() -> None:
    """
    Run the post-processing.

    :return: Nothing.
    """
    plot_eardrum_specific_impedance(dst=Path(__file__).parent.joinpath('plot'))
    post = process(data_dir=Path(__file__).parent.joinpath('output'))
    plot_post(post=post, dst=Path(__file__).parent.joinpath('plot'))
    animate(data_dir=Path(__file__).parent.joinpath('output'), anim_dir=Path(__file__).parent.joinpath('animation'))


if __name__ == '__main__':
    run()
