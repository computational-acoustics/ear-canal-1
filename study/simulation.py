import pyelmer.elmer as elmer
from study import impedance
from pathlib import Path
import subprocess
import shutil
import numpy as np
import json


def setup(
        coordinate_scaling: float,
        frequency: float,
        air_density: float,
        speed_of_sound: float,
        body_ids: list[int],
        inlet_ids: list[int],
        inlet_velocity: complex,
        rigid_ids: list[int],
        termination_ids: list[int],
) -> elmer.Simulation:
    """
    Set up an Elmer simulation for the ear canal. This returns a [`pyelmer.elmer.Simulation`][pyelmer.elmer.Simulation]
    object that can be used to deploy a SIF file in a directory where the mesh is present. The simulation uses the
    `HelmholtzSolver` solver with:

    * A normal velocity boundary condition applied to the ear canal input (inlet).
    * A rigid boundary condition applied to the ear canal walls.
    * An impedance boundary condition applied to the ear canal termination according to [Measurement of the eardrum impedance of human ears](https://pubmed.ncbi.nlm.nih.gov/6826891/) by Herbert Hudde.

    :param coordinate_scaling: The coordinate scaling, dimensionless.
    :param frequency: The frequency of the simulation, in Hertz.
    :param air_density: The density of air, in kilogram per cubic meter.
    :param speed_of_sound: The speed of sound in air, in meter per second.
    :param body_ids: The list of the body IDs to which apply the `HelmholtzSolver` solver.
    :param inlet_ids: The list of face IDs to which apply the normal velocity boundary condition.
    :param inlet_velocity: The normal velocity of the inlet surface, in meter per second.
    :param rigid_ids: The list of face IDs to which apply the rigid boundary condition.
    :param termination_ids: The list of face IDs to which apply the impedance boundary condition.
    :return: The Simulation object.
    """

    sim = elmer.Simulation()
    sim.settings = {
        'Max Output Level': 5,
        'Coordinate System': 'Cartesian',
        'Coordinate Mapping(3)': '1 2 3',
        'Simulation Type': 'Steady state',
        'Steady State Max Iterations': 1,
        'Output Intervals(1)': '1',
        'Coordinate Scaling': coordinate_scaling,
        'Frequency': frequency,
        'Solver Input File': 'case.sif',
        'Post File': 'case.vtu'
    }
    sim.constants = {
        'Gravity(4)': '0 -1 0 9.82',
        'Stefan Boltzmann': 5.670374419e-08,
        'Permittivity of Vacuum': 8.85418781e-12,
        'Permeability of Vacuum': 1.25663706e-6,
        'Boltzmann Constant': 1.380649e-23,
        'Unit Charge': 1.6021766e-19
    }

    air = elmer.Material(
        simulation=sim,
        name='Material-Air',
        data={
            'Sound speed': speed_of_sound,
            'Density': air_density
        }

    )

    solver = elmer.Solver(
        simulation=sim,
        name='Solver-Helmholtz',
        data={
            'Equation': 'Helmholtz Equation',
            'Procedure': '"HelmholtzSolve" "HelmholtzSolver"',
            'Variable': '-dofs 2 Pressure Wave',
            'Exec Solver': 'Always',
            'Stabilize': True,
            'Bubbles': False,
            'Lumped Mass Matrix': False,
            'Optimize Bandwidth': True,
            'Steady State Convergence Tolerance': 1.0e-5,
            'Nonlinear System Convergence Tolerance': 1.0e-7,
            'Nonlinear System Max Iterations': 1,
            'Nonlinear System Newton After Iterations': 3,
            'Nonlinear System Newton After Tolerance': 1.0e-3,
            'Nonlinear System Relaxation Factor': 1,
            'Linear System Solver': 'Iterative',
            'Linear System Iterative Method': 'BiCGStabl',
            'Linear System Max Iterations': 1000,
            'Linear System Convergence Tolerance': 1.0e-10,
            'BiCGstabl polynomial degree': 2,
            'Linear System Preconditioning': 'ILUT',
            'Linear System ILUT Tolerance': 1.0e-3,
            'Linear System Abort Not Converged': True,
            'Linear System Residual Output': 10,
            'Linear System Precondition Recompute': 1,
            'Element': 'p:2'
        }
    )

    equation = elmer.Equation(
        simulation=sim,
        name='Equation-Helmholtz',
        solvers=[solver],
        data={
            'Frequency': frequency
        }
    )

    body = elmer.Body(
        simulation=sim,
        name='Body-Air',
        body_ids=body_ids,
    )
    body.material = air
    body.equation = equation

    inlet = elmer.Boundary(
        simulation=sim,
        name='Inlet',
        geo_ids=inlet_ids,
        data={
            'Wave Flux 1': inlet_velocity.real,
            'Wave Flux 2': inlet_velocity.imag,
        }
    )

    rigid = elmer.Boundary(
        simulation=sim,
        name='Rigid',
        geo_ids=rigid_ids,
        data={
            'Wave Flux 1': 0,
            'Wave Flux 2': 0,
        }
    )

    elmer_termination_impedance = impedance.elmer_eardrum_impedance(
        frequency=frequency, 
        speed_of_sound=speed_of_sound
    )

    termination = elmer.Boundary(
        simulation=sim,
        name='Termination',
        geo_ids=termination_ids,
        data={
            'Wave Impedance 1': elmer_termination_impedance.real,
            'Wave Impedance 2': elmer_termination_impedance.imag,
        }
    )

    return sim


def run_sim(sim: elmer.Simulation, dst: Path) -> None:
    """
    Run an ear canal simulation. This function will take a [`pyelmer.elmer.Simulation`][pyelmer.elmer.Simulation] and
    deploy it as a SIF in a target directory. The mesh for the simulation, contained within this software package,
    will also be deployed in `ElmerGrid` format.

    :param sim: The simulation object.
    :param dst: The deployment directory.
    :return: Nothing.
    """
    assert dst.is_dir()
    dst_str = dst.absolute().as_posix()

    sim.write_startinfo(simulation_dir=dst_str)
    sim.write_sif(simulation_dir=dst_str)

    msh_path = Path(__file__).parent.joinpath('data', 'mesh', 'mesh_20kHz.unv')

    assert subprocess.run(
        ['ElmerGrid', '8', '2', msh_path.absolute().as_posix(), '-autoclean'],
        cwd=dst
    )

    msh_dir = msh_path.parent.joinpath(msh_path.stem)

    for p in msh_dir.glob('*.*'):
        shutil.move(src=p, dst=dst.joinpath(p.name))

    shutil.rmtree(msh_dir)

    assert subprocess.run(
        ['ElmerSolver'],
        cwd=dst
    )


def run(
        frequency: float,
        inlet_velocity: complex,
        dst: Path
) -> None:
    """
    Set up and run an ear canal simulation over the mesh bundled with this package, assuming air at room temperature.

    The simulation uses the `HelmholtzSolver` solver with:

    * A normal velocity boundary condition applied to the ear canal input (inlet).
    * A rigid boundary condition applied to the ear canal walls.
    * An impedance boundary condition applied to the ear canal termination according to [Measurement of the eardrum impedance of human ears](https://pubmed.ncbi.nlm.nih.gov/6826891/) by Herbert Hudde.

    :param frequency: The frequency of the simulation, in Hertz.
    :param inlet_velocity: The normal velocity of the ear canal inlet, in meter per second.
    :param dst: The deployment directory.
    :return: Nothing.
    """

    sim = setup(
        coordinate_scaling=0.001,
        frequency=frequency,
        air_density=1.205,
        speed_of_sound=343.0,
        body_ids=[1, ],
        inlet_ids=[1, ],
        inlet_velocity=inlet_velocity,
        rigid_ids=[3, ],
        termination_ids=[2, ],
    )

    run_sim(sim=sim, dst=dst)


if __name__ == '__main__':
    # Making many simulations, each at a different frequency.
    frequencies = np.unique(np.round(np.logspace(3, np.log10(20e3), num=2048)))
    vel = 1.0 + 0.0j

    for freq in frequencies:
        sim_dir = Path(__file__).parent.joinpath('output', f'{freq}_Hz_{vel}_mps')
        sim_dir.mkdir(parents=True, exist_ok=True)

        run(
            frequency=freq,
            inlet_velocity=vel,
            dst=sim_dir
        )

        # The basic simulation properties are dumped into a JSON file to make post-processing easier.
        with open(sim_dir.joinpath('simdata.json'), 'w') as file:
            json.dump(
                {
                    'frequency': freq,
                    'velocity_re': vel.real,
                    'velocity_im': vel.imag
                },
                file
            )
